# RestAssured_Framework_Design


RestAssured is a library, where we can trigger API and capture response.

Framework is to maintain all project related files.

**objectives of framework :**
    Resusability and
    Maintainability

Framework consists of both **Keyword driven**-  and **Data driven**.

**Structure of  framework**

**1.Framework design type :**

Static  - Directly importing all the testclass into single main method and we have to explicitly write the testclass name and executing it.

Dynamic  - By using, java.lang.reflect dynamically calling the testclass from API package and executing it.

**2.TestClass:**

In API Package, For each API’s  I have separate testclass and multiple testcases for each testclass.

**3.Common method components:**

For configuring and triggering the API using
Given(), When() and Then()

**i) API components:** 

Request,endpoint and common functions to trigger API and extract statuscode and responsebody

**ii)Utility function Package:**



a)To get test logfiles, we are creating directory,if it is not exists.
If directory exists,we are deleting and creating it.

b) To create logfiles and endpoint,requestbody,reponsebody and its respective headers added into notepad file of each testclass.

c)Excel data extractor is to read the test datas from an excel file.

d)ExtentListener class – In this class I had implemented ITestListener and used its own methods.

By using listener tag in xml, will track the test execution such as starting of testcase,testcase failure etc.,

**4.Test Data File:**

It consists of  requestbody parameters entered into excel file.

**5.Libraries:**

Below given are the dependencies added in pom.xml and handled by Maven repository

a)RestAssured – To trigger API and extract statuscode and responsebody

b)TestNG – To validate the reponse body using Assert class, organise and  run testcases through testNG

c)JsonPath – To parse the response

d)Apache.poi – To read data from an excel file

e)Allure

f)Extentreport

**6.Reports:**

Extent report – User friendly,widely used and detailed report







