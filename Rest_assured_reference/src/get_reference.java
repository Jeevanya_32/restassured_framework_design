import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import org.json.JSONObject;
import org.json.JSONArray;
import org.testng.Assert;
public class get_reference {

	public static void main(String[] args) {
		int expected_id[]={1,2,3,4,5,6};
	    String expected_email[]={"george.bluth@reqres.in", "janet.weaver@reqres.in",
	                             "emma.wong@reqres.in","eve.holt@reqres.in",
	                             "charles.morris@reqres.in","tracey.ramos@reqres.in"};
	    String expected_first_name[]= {"George","Janet","Emma","Eve","Charles","Tracey"};
	    String expected_last_name[]= {"Bluth","Weaver","Wong","Holt","Morris","Ramos"};

//step 1 Declare base URL
		RestAssured.baseURI="https://reqres.in/";
		
// step 2 configure rest parameters and trigger API
		String responsebody = given().when().get("api/users?page=1").then().extract().response().asString();
		System.out.println("Responsebody is :" +responsebody);
		
//step 3 create an object 
		JsonPath jsp_res=new JsonPath(responsebody);
		
//step 4 To extract the array
		JSONObject array_res=new JSONObject(responsebody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);
		
//step 5 To findout the dataarray length
		int count=dataarray.length();
		System.out.println(count);
		
		for (int i=0; i<count; i++) {
			int res_id=dataarray.getJSONObject(i).getInt("id");
			String res_email=dataarray.getJSONObject(i).getString("email");
			String res_firstname=dataarray.getJSONObject(i).getString("first_name");
			String res_lastname=dataarray.getJSONObject(i).getString("last_name");
			
		    int exp_id=expected_id[i];
		    String exp_email=expected_email[i];
		    String exp_firstname=expected_first_name[i];
		    String exp_lastname=expected_last_name[i];
			
//step 6 validate response body
		Assert.assertEquals(res_id,exp_id);
		Assert.assertEquals(res_email,exp_email);
		Assert.assertEquals(res_firstname,exp_firstname);
		Assert.assertEquals(res_lastname,exp_lastname);
		
		}	
	}
}


