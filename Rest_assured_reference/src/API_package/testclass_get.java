package API_package;

import API_common_methods.common_method_handle_API;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Get_APIendpoint;
import io.restassured.path.json.JsonPath;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

public class testclass_get extends common_method_handle_API {
	@Test
	public static void executor() throws IOException {
		File log_dir=Directory_handle.log_directory_creation("API_getlogs");
        String endpoint=Get_APIendpoint.endpoint_get();
 
     for (int i=0; i<5; i++) {
	    int statuscode = get_statuscode(endpoint);
	    System.out.println(statuscode);
	    if (statuscode ==200) {
	    String responsebody = get_responsebody(endpoint);
	    System.out.println(responsebody); 
	    handle_API_logs.evidence_creator(log_dir,"testclass_get",endpoint,responsebody);
	    testclass_get.validator(responsebody);
	    break;
        }
	    else {
	    	System.out.println("Incorrect statuscode is found hence retry");
	    }
    }
}
	public static void validator(String responsebody) {
		int expected_id[]={1,2,3,4,5,6};
	    String expected_email[]={"george.bluth@reqres.in", "janet.weaver@reqres.in",
	                             "emma.wong@reqres.in","eve.holt@reqres.in",
	                             "charles.morris@reqres.in","tracey.ramos@reqres.in"};
	    String expected_first_name[]= {"George","Janet","Emma","Eve","Charles","Tracey"};
	    String expected_last_name[]= {"Bluth","Weaver","Wong","Holt","Morris","Ramos"};

        JsonPath jsp_res=new JsonPath(responsebody);
		JSONObject array_res=new JSONObject(responsebody);
		JSONArray dataarray = array_res.getJSONArray("data");
		int count=dataarray.length();
		
		
		for (int i=0; i<count; i++) {
			int res_id=dataarray.getJSONObject(i).getInt("id");
			String res_email=dataarray.getJSONObject(i).getString("email");
			String res_firstname=dataarray.getJSONObject(i).getString("first_name");
			String res_lastname=dataarray.getJSONObject(i).getString("last_name");
			
		    int exp_id=expected_id[i];
		    String exp_email=expected_email[i];
		    String exp_firstname=expected_first_name[i];
		    String exp_lastname=expected_last_name[i];
	
		    Assert.assertEquals(res_id,exp_id);
		    Assert.assertEquals(res_email,exp_email);
		    Assert.assertEquals(res_firstname,exp_firstname);
		    Assert.assertEquals(res_lastname,exp_lastname);
	}
   }
  }
