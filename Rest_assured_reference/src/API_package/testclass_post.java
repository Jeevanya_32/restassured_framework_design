package API_package;
import API_common_methods.common_method_handle_API;
import apirequest_repository.PostAPI_request_repository;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Post_APIendpoint;
import io.restassured.path.json.JsonPath;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
public class testclass_post extends common_method_handle_API  {
	@Test
	public static void executor() throws IOException {
			File log_dirname=Directory_handle.log_directory_creation("API_postlogs");
		    String requestbody = PostAPI_request_repository.post_req();
		    String endpoint = Post_APIendpoint.endpoint_post();
		    
		    for (int i=0; i<5; i++) {
				 int statuscode = post_statuscode(requestbody,endpoint);
				 System.out.println(statuscode);
				 
			 if  (statuscode ==201) {
					 String responsebody = post_responsebody(requestbody,endpoint);
					 System.out.println(responsebody);
					 handle_API_logs.evidence_creator(log_dirname,"testclass_post",endpoint, requestbody, responsebody);
					 testclass_post.validator(requestbody,responsebody);
					 break;
				 }
			 else
				 {
					 System.out.println("Incorrect statuscode is found hence retry");
				 }
		    }
	}

public static void validator(String requestbody,String responsebody) {
	
	JsonPath jsp_req=new JsonPath(requestbody);
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");
	
	JsonPath jsp_res=new JsonPath(responsebody);
	String res_name = jsp_res.getString("name");
	String res_job = jsp_res.getString("job");
	String res_id = jsp_res.getString("id");
	LocalDateTime currentdate = LocalDateTime.now();
	String expected_date = currentdate.toString().substring(0, 11);
	String res_createdat = jsp_res.getString("createdAt");
	res_createdat = res_createdat.substring(0, 11);
	
	Assert.assertEquals(res_name,req_name);
	Assert.assertEquals(res_job,req_job);
	Assert.assertNotNull(res_id);
	Assert.assertEquals(res_createdat,expected_date);
	}
}