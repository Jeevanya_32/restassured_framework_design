package common_methods_utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extractor {

public static ArrayList<String> Excel_data_reader(String filename, String sheetname, String Testclass_name)
		throws IOException {
		ArrayList<String> Arraydata = new ArrayList<String>();
		String project_directory = System.getProperty("user.dir");
		System.out.println("Current project directory path is " + project_directory);
//step 1: Create the object of file input stream to locate the data file
		FileInputStream FIS = new FileInputStream(project_directory + "\\API_DataFiles\\" + filename + ".xlsx");
//step 2: Create XSSFWorkbook object to open excel file
		XSSFWorkbook WB = new XSSFWorkbook(FIS);
//step 3: Fetch no.of.sheets available in excel file
		int count = WB.getNumberOfSheets();
		//System.out.println(count);
//Step 4: Access the sheet as per input sheet name
		for (int i = 0; i < count; i++) {
			String Sheetname = WB.getSheetName(i);

			if (Sheetname.equals(sheetname)) {
				System.out.println(sheetname);
				XSSFSheet sheet = WB.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row datarow = row.next();
					String testclassname = datarow.getCell(0).getStringCellValue();
					if (testclassname.equals(Testclass_name)) {
						Iterator<Cell> cellvalues = datarow.iterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							// System.out.println(testdata);
							Arraydata.add(testdata);
						}
					}
				}
			}
		}
		WB.close();
		return Arraydata;
      }
}
