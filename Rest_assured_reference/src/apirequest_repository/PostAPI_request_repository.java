package apirequest_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_methods_utility.Excel_data_extractor;

public class PostAPI_request_repository {
public static String post_req() throws IOException{
	ArrayList<String> data=Excel_data_extractor.Excel_data_reader("TestdataAPI","Post_API","Post_TC1");
	String name=data.get(1);
	String job=data.get(2);
	  String requestbody="{\r\n"
            + "    \"name\": \""+name+"\",\r\n"
            + "    \"job\": \""+job+"\"\r\n" + "}";
	  return requestbody;
}
}
