package driver_package;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import common_methods_utility.Excel_data_extractor;

public class Dynamic_driverclass {

	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> Testcase_execute = Excel_data_extractor.Excel_data_reader("TestdataAPI", "TestCases",
				"Testclass_name");
		System.out.println(Testcase_execute);
		int count = Testcase_execute.size();
		for (int i = 1; i < count; i++) {
			String Testclass_name = Testcase_execute.get(i);
			System.out.println(Testclass_name);

//call the testclass name on runtime by using java.lang.reflect package
			Class<?> Test_class = Class.forName("API_package." + Testclass_name);

//call the execute method belonging to testclass captured in variable Testclass_name by using java.lang.reflect method class
			Method execute_method = Test_class.getDeclaredMethod("executor");

//Set accessibility of method true
			execute_method.setAccessible(true);

//create instance of testclass captured in variable name Testclass_name
			Object instance_of_testclass = Test_class.getDeclaredConstructor().newInstance();

//Execute the testscript class fetched in variable Test_class
			execute_method.invoke(instance_of_testclass);
		}

	}

}
