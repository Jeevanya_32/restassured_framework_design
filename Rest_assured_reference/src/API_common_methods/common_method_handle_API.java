package API_common_methods;
import static io.restassured.RestAssured.given;
public class common_method_handle_API {
	                           //POST
public static int post_statuscode(String requestbody,String endpoint) {
	
	int statuscode = given().header("Content-Type","application/json").body(requestbody)
			        .when().post(endpoint).then().extract().statusCode();
	                return statuscode;
}
public static String post_responsebody(String requestbody,String endpoint) {
	String responsebody = given().header("Content-Type","application/json").body(requestbody)
			              .when().post(endpoint).then().extract().response().asString();
	                      return responsebody;
}
                               //PUT
public static int put_statuscode(String requestbody,String endpoint) {
	int statuscode = given().header("Content-Type","application/json").body(requestbody)
			        .when().put(endpoint).then().extract().statusCode();
	                 return statuscode;
}
public static String put_responsebody(String requestbody,String endpoint) {
	String responsebody = given().header("Content-Type","application/json").body(requestbody)
			              .when().put(endpoint).then().extract().response().asString();
	                       return responsebody;
}
                               //PATCH
public static int patch_statuscode(String requestbody,String endpoint) {
	
	int statuscode = given().header("Content-Type","application/json").body(requestbody)
			        .when().patch(endpoint).then().extract().statusCode();
	                 return statuscode;
}
public static String patch_responsebody(String requestbody,String endpoint) {
	String responsebody = given().header("Content-Type","application/json").body(requestbody)
			              .when().patch(endpoint).then().extract().response().asString();
	                      return responsebody;
}
                                //GET
public static int get_statuscode(String endpoint) {
	
	int statuscode = given().header("Content-Type","application/json")
			        .when().get(endpoint).then().extract().statusCode();
	                 return statuscode;
}
public static String get_responsebody(String endpoint) {
	String responsebody = given().header("Content-Type","application/json")
			              .when().get(endpoint).then().extract().response().asString();
	                      return responsebody;
}
                              //Delete
public static int delete_statuscode(String endpoint) {
	
	int statuscode = given().header("Content-Type","application/json")
			        .when().delete(endpoint).then().extract().statusCode();
	                 return statuscode;
	
}
}

