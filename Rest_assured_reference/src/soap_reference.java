import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
public class soap_reference {

	public static void main(String[] args) {
		//step 1 :declare baseURL
		RestAssured.baseURI="https://www.dataaccess.com";
		//step 2 :declare request body
		 String requestbody="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
		 		+ "   <soapenv:Header/>\r\n"
		 		+ "   <soapenv:Body>\r\n"
		 		+ "      <web:NumberToWords>\r\n"
		 		+ "         <web:ubiNum>440</web:ubiNum>\r\n"
		 		+ "      </web:NumberToWords>\r\n"
		 		+ "   </soapenv:Body>\r\n"
		 		+ "</soapenv:Envelope>";
		//step3 Trigger the API and fetch the response body
String responsebody = given().header("Content-Type","text/xml; charset=utf-8").body(requestbody).
when().post("webservicesserver/NumberConversion.wso").
then().extract().response().getBody().asString();
        //Step 4 : print the response body
          System.out.println(responsebody);
          //step 5 : create an object and extract response body
          XmlPath Xml_res=new XmlPath(responsebody);
          String res_tag=Xml_res.getString("NumberToWordsResult");
  		  System.out.println(res_tag);
  		//step6 Validate the response body
  		  Assert.assertEquals(res_tag,"four hundred and forty ");
          }
         }
