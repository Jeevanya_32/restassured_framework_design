import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
public class delete_reference {

	public static void main(String[] args) {
//step 1 Declare base URL
		RestAssured.baseURI="https://reqres.in/";
		
// step 2 configure rest parameters and trigger API
String responsebody = given().header("Content-Type","application/json").
				when().delete("api/users/2").
				then().log().all().extract().response().asString();
		//System.out.println("Responsebody is :" +responsebody);
	}

}



