import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;
public class patch_reference {
	public static void main(String[] args) {
//step 1 Declare base URL
		RestAssured.baseURI="https://reqres.in/";
		
// step 2 configure rest parameters and trigger API
		String requestbody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
        String responsebody = given().header("Content-Type","application/json").
			body(requestbody).when().patch("api/users/2").
				then().log().all().extract().response().asString();
		//System.out.println("Responsebody is :" +responsebody);
        
//step 3:create an object of json path to parse the request body
JsonPath jsp_req=new JsonPath(requestbody);
String req_name=jsp_req.getString("name");
String req_job=jsp_req.getString("job");

//step 4:create an object of json path to parse response body
JsonPath jsp_res = new JsonPath(responsebody);
String res_name = jsp_res.getString("name");
String res_job = jsp_res.getString("job");
LocalDateTime currentdate = LocalDateTime.now();
System.out.println(currentdate);
String expected_date = currentdate.toString().substring(0,11);
String res_updatedat = jsp_res.getString("updatedAt");
res_updatedat = res_updatedat.substring(0,11);

//Step 5:validate responsebody
Assert.assertEquals(res_name,req_name);
Assert.assertEquals(res_job,req_job);
Assert.assertEquals(res_updatedat,expected_date);
	}
}



