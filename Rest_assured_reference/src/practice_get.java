import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import org.json.JSONObject;
import org.json.JSONArray;
import org.testng.Assert;
public class practice_get {

	public static void main(String[] args) {
		int expected_id[]= {5658233,5658229, 5658228,5658224,5658220,5658219,
				5658218,5658216,5658206,5658204};
		String expected_name[]= {"Samir Panicker","Bilva Gandhi","Adripathi Ahuja","Rep. Ananda Adiga","Prasanna Johar",
				"Chidambar Saini","Kama Kapoor Jr.", "Bhadra Prajapat","Balamani Abbott","Rep. Charvi Dutta"};
		//1 declare baseURI
		RestAssured.baseURI="https://gorest.co.in";
		//2 configure rest parameter and trigger api
String responsebody=given().when().get("public/v2/users?gender=male").then().extract().response().asString();
//create object
JsonPath jsp_res=new JsonPath(responsebody);

JSONObject array_res=new JSONObject(responsebody);
JSONArray dataarray=new JSONArray(responsebody);
int count=dataarray.length();
System.out.println(count);

for(int i=0; i<count; i++) {
	int res_id=dataarray.getJSONObject(i).getInt("id");
	String res_name=dataarray.getJSONObject(i).getString("name");
	
	int exp_id=expected_id[i];
	String exp_name=expected_name[i];
	
	Assert.assertNotNull(res_id);
	Assert.assertNotNull(res_name);
}
	}

}
